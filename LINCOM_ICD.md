# LINCOM Interface Control Document
## For protocol version 1.0, *2017-12-28*

# LINCOM Packet
A lincom packet consists of a header, a payload descriptor, then a payload.
The header consists of a 4 character string, always _"LCOM"_ and the major and minor version numbers as unsigned shorts.
The header will always remain constant.
The payload descriptor may change with major version updates.
With versions 1.0, the payload descriptor contains a timestamp, a message ID and the message data length.

The actual payload data is serialized into bytes that is different for each message type.

# LINCOM Messages

Each LINCOM message is identified by both a numeric ID and a 4 character identification code.

## ECHO Message

The ECHO message is used to test connectivity to a client.
The contents are a single unsigned integer.
The response is this unsigned integer with the least significant bit flipped.

## RESP Message

A RESP message contains the server response for a given action.
The contents are a single unsigned short.

## CHAT Message

The chat message is the most common form of message sent over LINCOM.
It describes a message being sent from one client to another.
This message is relayed through the server to other connected clients.

### Payload
The payload consists of three fields:
* A string source
* A string destination
* A string with the message.

Strings are terminated with a null byte.

### Return Codes
* **E_DEST_NOT_FOUND** - Could not find the destination

## NICK Message

The NICK message sets the client nickname on the server.

### Payload
The payload consists of one null terminated string that contains the nickname to request.

### Return Codes

* **E_INVALID_NICKNAME** - Nickname is invalid.
* **E_NICKNAME_IN_USE** - Nickname is already taken.

# Response Codes

## Success codes
Success codes are returned when the command was completed successfully.
* **S_OK**: Command was successful

## Warning codes
Warning codes are returned when the command was executed, but may not have been executed as intended.

## Error codes
Error codes are returned when the command had no effect, or was rolled back.

* **E_DEST_NOT_FOUND** - Could not find the destination
* **E_INVALID_NICKNAME** - Nickname is invalid.
* **E_NICKNAME_IN_USE** - Nickname is already taken.
