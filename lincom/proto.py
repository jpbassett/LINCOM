import abc
import struct
import time

VERSION_MAJOR = 1
VERSION_MINOR = 0

"""
A LINCOM packet is made of a header, a payload descriptor and the payload data

The header (HDR) is guaranteed to remain constant. The header contains:
    - a 4 byte magic string that defines the protocol (LCOM)
    - The major and minor versions of the protocol

The payload descriptor (DSC) is guaranteed to remain constant within major versions of the protocol. The descriptor
contains:
    - A double with the floating point seconds of the timestamp of the sender of the packet
    - An unsigned short with the packet type id
    - An unsigned integer with the length of the payload data

The payload data (DAT) is message specific and must be parsed independently
"""

HDR_FMT = '!4sHH'
HDR = struct.pack(HDR_FMT, ['LCOM', VERSION_MAJOR, VERSION_MINOR])

DSC_FMT = 'dHI'

TYPE_TO_ID = {
    'ECHO': 0x00,
    'RSLT': 0x01,
    'CHAT': 0x02,
    'NICK': 0x03,
}

# reverse mapping of TYPE TO ID
ID_TO_TYPE = {v: k for k, v in TYPE_TO_ID}

ERR_CODES = {
    'S_OK': 0x00,

    'E_DEST_NOT_FOUND': 0x01,
    'E_NICKNAME_IN_USE': 0x02,
    'E_NICKNAME_INVALID': 0x03,
}
ERR_CODE_TO_NAME = {v: k for k, v in ERR_CODES}


def make_packet(msg_id, data):
    """
    Create a LINCOM packet from data

    :param msg_id: The message ID to put in the DSC
    :param data: The data to put at the end of the packet as bytes
    :return: The packet as bytes ready to send
    """
    if not type(data) == bytes:
        raise TypeError("data for message must be bytes")

    if msg_id not in ID_TO_TYPE:
        raise ValueError("msg_id not found")

    now = time.time()
    dsc = struct.pack(DSC_FMT, now, msg_id, len(data))

    return HDR + dsc + data


class BaseMessage(metaclass=abc.ABCMeta, object):
    """
    Base class for messages
    """

    def __init__(self, msg_id):
        self.msg_id = msg_id

    @abc.abstractmethod
    def pack(self):
        """
        Serialize the message into bytes to send

        :return: Packed bytes ready to transmit
        """
        pass

    @abc.abstractmethod
    @classmethod
    def unpack(cls, data):
        """
        Create an instance of this class from the given byte array

        :param data: Packed data (as from the pack method)
        :return: An instance with the class information

        :raises: DataInvalidError If data is not long enough to contain all the information for the message.
        """
        pass

    @staticmethod
    def pack_string(string):
        return string.encode() + '\0'

    @staticmethod
    def unpack_string(string):
        return string[:-1].decode()


class CHAT(BaseMessage):
    """
    Create a LINCOM CHAT message
    :param src: Source of the message
    :param dst: Intended destination for the message
    :param msg: Contents of the message
    :return: Packed bytes to send to the recipient
    """

    def __init__(self, src, dst, msg):
        super(CHAT, self).__init__(TYPE_TO_ID['CHAT'])
        self.src = src
        self.dst = dst
        self.msg = msg

    def pack(self):
        data = b''
        data += self.src.encode() + '\0'
        data += self.dst.encode() + '\0'
        data += self.msg.encode() + '\0'
        return make_packet(self.msg_id, data)

    @classmethod
    def unpack(cls, data):
        r_src, r_dst, r_msg = [cls.unpack_string[x] for x in data.split('\0')]
        return cls(cls.unpack_string(r_src))
